// contains functions and business logic of our express js application
// all operations it can do will be placed in this file.
// allows us to use the contents of "task.js" file in the "models" folder
const Task = require("../models/task.js");

// defines the functions to be used in the "taskRoutes.js"
module.exports.getAllTasks = () => {
	return Task.find( {  } ).then(result => {
		return result;
	});
};

// function for creating a task
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	// waits for the save method to complete before detecting any errors
	return newTask.save().then((task, error) => {
		if (error){
			console.log(error);
			return false;	
		}else{
			return task;
		}
	})
}

// function for deleting a task
/*
	Look for the task using "findyByIdAndRemove" with the corresponding id provided/passed from the URI/route

	Delete the task 
		-wait for the deletion to complete and then if there are any errors, print in the console the error,
		- if there are no errors, return the removed task

	5 mins - 8:01
	send the Postman output in the google chat
*/
module.exports.deleteTask = (taskId) => {
	// findByIdAndRemove - is a mongoose method that searches for the documents using the _id properties; after finding a document, the job of this method is to remove the document
	return Task.findByIdAndRemove(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return result
		}
	})
}


// Updating a task
// Business logic
/*
	1. get the task with the id using the mongoose method "findById"
	2. Replace the task's name returned from the database with the "name" property from the request body
	3. save the task
*/
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			result.name = newContent.name;
			return result.save().then((updatedTask, error)=>{
				if (error){
					console.log(error);
					return false
				}else{
					return updatedTask;
				};
			});
		};
	});
};


// ACTIVITY 

// Controller function for getting a specific task

	// Business Logic
	/*
		1. Get the task with the id using the Mongoose method "findById"
	*/




// Controller function for updating a task status to "complete"

	// Business Logic
	/*
		1. Get the task with the id using the Mongoose method "findById"
		2. Change the status of the document to complete (hardcoded OR using requestBody)
		3. Save the task
	*/

