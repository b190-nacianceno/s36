// contains all endpoints for our application
// app.js/index.js should only be concerned with information about the server
// we have to use the ROuter method inside express

const express = require("express");

// allows us to access HTTP method middlewares that make it easier to create routing system
const router = express.Router();


// this allows us to use the contents of "taskControllers.js" in "controllers" folder
const taskController = require("../controllers/taskController.js");
const task = require("../models/task.js");

// route to get all tasks 

/* 
get request to be sent at localhost:3000/tasks
*/
router.get("/",(req,res) => {
    // since the controller did not send any response, the task falls to the routes to send a response that is received from the controllers  and send it to the frontend
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/",(req,res) => {
    // since the controller did not send any response, the task falls to the routes to send a response that is received from the controllers  and send it to the frontend
    console.log(req.body);
    taskController.createTask(req.body).then(result => res.send(result));
});

// route for deleting a task
router.delete("/delete/:id", (req,res) => {
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});


// route for updating a task
router.put("/updatetask/:id",(req,res) => {
    /* 
    req.params.id retrieves the taskId of the document to be updated
    req.body determines what updates are to be done inthe documents coming form the request's body
    */
    taskController.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
});

// SECTION -ACTIVITY
// create a route for getting a specific task
router.get("/getonetask/:id",(req,res) => {
    
    taskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

// route for changing the status of a task to complete
router.put("/:id/complete",(req,res) => {
    
    taskController.updateStatus(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
});


// module.exports = allows us to export the file where it is inserted to be used by other files such as app.js/index.

module.exports = router;