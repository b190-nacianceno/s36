// npm init --yes

// gitignore - node_modules
// npm install express
// npm install mongoose -g 
// npm start




const express = require("express");

const mongoose = require("mongoose");

// allows us to use contents of "taskRoutes.js" in the "routes" folder
const taskRoutes = require("./routes/taskRoutes.js");

// Server setup
const app = express();
const port = 3000;

// MongoDB connection
mongoose.connect("mongodb+srv://ezranacianceno:Bernadette12@cluster0.mliwlr2.mongodb.net/b190-to-do?retryWrites=true&w=majority",
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open",() => console.log("we're connected to the database"));


app.use(express.json());
app.use(express.urlencoded({extended:true}));


// allows all the task routes created in the "taskRoutes.js" files to use "/tasks" route
app.use("/tasks",taskRoutes);


app.listen(port, () => console.log(`Server running at port: ${port}`));