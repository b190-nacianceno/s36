// model files should import the mongoose modules
const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
});

// modules.exports - allows us to export the file where it is inserted to bused by other files like app.js/index.js
module.exports = mongoose.model("Task", taskSchema);